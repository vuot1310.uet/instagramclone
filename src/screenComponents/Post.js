import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, FlatList, ScrollView } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Post = () => {
  const postInfos = [
    {
      index: 1,
      postTitle: 'post 1',
      postPersonImage: require('../../assets/storage/images/userProfile.png'),
      postImage: require('../../assets/storage/images/post1.jpg'),
      likes: 765,
      isLiked: false,
    },
    {
      index: 2,
      postTitle: 'post 2',
      postPersonImage: require('../../assets/storage/images/profile1.jpg'),
      postImage: require('../../assets/storage/images/post2.jpg'),
      likes: 765,
      isLiked: false,
    },
    {
      index: 3,
      postTitle: 'post 3',
      postPersonImage: require('../../assets/storage/images/profile2.jpg'),
      postImage: require('../../assets/storage/images/post3.jpg'),
      likes: 765,
      isLiked: false,
    },
    {
      index: 4,
      postTitle: 'post 4',
      postPersonImage: require('../../assets/storage/images/profile3.jpg'),
      postImage: require('../../assets/storage/images/post4.jpg'),
      likes: 765,
      isLiked: false,
    },
    {
      index: 5,
      postTitle: 'post 5',
      postPersonImage: require('../../assets/storage/images/profile4.jpg'),
      postImage: require('../../assets/storage/images/post5.jpg'),
      likes: 765,
      isLiked: false,
    },
  ];

  const ViewItem = ({data}) => {
    const [like, setLike] = useState(data.isLiked);
    return (
        <View
          key={data.index}
          style={{
            paddingBottom: 10,
            borderBottomColor: 'gray',
            borderBottomWidth: 0.1,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: 15,
            }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                source={data.postPersonImage}
                style={{
                  width: 40,
                  height: 40,
                  borderRadius: 100,
                }}
              />
              <View
                style={{
                  paddingLeft: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: '700',
                  }}
                >
                  {data.postTitle}
                </Text>
              </View>
            </View>
            <Feather name="more-vertical" style={{ fontSize: 20 }} />
          </View>
          <View
            style={{
              position: 'relative',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Image source={data.postImage} style={{ width: '100%', height: 350 }} />
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 12,
              paddingVertical: 15,
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <TouchableOpacity onPress={() => setLike(!like)}>
                <AntDesign
                  name={like ? 'heart' : 'hearto'}
                  style={{
                    paddingRight: 10,
                    fontSize: 20,
                    color: like ? 'red' : 'black',
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Ionicon
                  name="ios-chatbubble-outline"
                  style={{
                    fontSize: 20,
                    paddingRight: 10,
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Feather
                  name="navigation"
                  style={{
                    fontSize: 20,
                  }}
                />
              </TouchableOpacity>
            </View>
            <Feather name="bookmark" style={{ fontSize: 20 }} />
          </View>
          <View>
            <Text>
              Liked by {like ? 'you and' : ''} {like ? data.likes + 1 : data.likes} others
            </Text>
            <Text
              style={{
                opacity: 0.4,
                paddingVertical: 2,
              }}
            >
              View all comments
            </Text>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                alignItems: 'center',
              }}
            >
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Image
                  source={data.postPersonImage}
                  style={{
                    width: 25,
                    height: 25,
                    borderRadius: 100,
                    backgroundColor: 'orange',
                    marginRight: 10,
                  }}
                />
                <TextInput
                  placeholder="Add a comment"
                  style={{
                    opacity: 0.5,
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  display: 'flex',
                }}
              >
                <Entypo
                  name="emoji-happy"
                  style={{
                    fontSize: 15,
                    color: 'lightgreen',
                    marginRight: 10,
                  }}
                />
                <Entypo
                  name="emoji-neutral"
                  style={{
                    fontSize: 15,
                    color: 'pink',
                    marginRight: 10,
                  }}
                />
                <Entypo
                  name="emoji-sad"
                  style={{
                    fontSize: 15,
                    color: 'red',
                    marginRight: 10,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      );
  };
  return (
    // <FlatList 
    //     data={postInfos}
    //     renderItem={({item}) => <ViewItem data={item} />}
    //     keyExtractor={item => item.index}
    // />
    <ScrollView>
      {
        postInfos.map((data, index) => <ViewItem data={data} key={index} />)
      }
    </ScrollView>
  );
};

export default Post;
