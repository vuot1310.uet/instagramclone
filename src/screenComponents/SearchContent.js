import { View, Text, TouchableOpacity, Image } from 'react-native';

const SearchContent = (props) => {
  const searchData = [
    {
      id: 0,
      images: [
        require('../../assets/storage/images/post1.jpg'),
        require('../../assets/storage/images/post2.jpg'),
        require('../../assets/storage/images/post3.jpg'),
        require('../../assets/storage/images/post4.jpg'),
        require('../../assets/storage/images/post5.jpg'),
        require('../../assets/storage/images/post6.jpg'),
      ],
    },
    {
      id: 1,
      images: [
        require('../../assets/storage/images/post7.jpg'),
        require('../../assets/storage/images/post8.jpg'),
        require('../../assets/storage/images/post9.jpg'),
        require('../../assets/storage/images/post10.jpg'),
        require('../../assets/storage/images/post11.jpg'),
        require('../../assets/storage/images/post12.jpg'),
      ],
    },
    {
      id: 2,
      images: [
        require('../../assets/storage/images/post13.jpg'),
        require('../../assets/storage/images/post14.jpg'),
        require('../../assets/storage/images/post15.jpg'),
      ],
    },
  ];
  return (
    <View
      style={{
        position: 'relative',
        top: 40,
      }}
    >
      {searchData.map((data, index) => {
        return (
          <>
            {data.id === 0 && (
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'space-between',
                }}
              >
                {data.images.map((imageData, imgIndex) => {
                  return (
                    <TouchableOpacity
                      onPressIn={() => props.getData(imageData)}
                      onPressOut={() => props.getData(null)}
                      style={{ paddingBottom: 2 }}
                    >
                      <Image
                        source={imageData}
                        style={{
                          width: 120,
                          height: 150,
                        }}
                      />
                    </TouchableOpacity>
                  );
                })}
              </View>
            )}
            {data.id === 1 && (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    width: 261,
                    justifyContent: 'space-between',
                  }}
                >
                  {data.images.slice(0, 4).map((imageData, imgIndex) => {
                    return (
                      <TouchableOpacity
                        onPressIn={() => props.getData(imageData)}
                        onPressOut={() => props.getData(null)}
                        style={{ paddingBottom: 2 }}
                      >
                        <Image
                          source={imageData}
                          style={{
                            width: 129,
                            height: 150,
                          }}
                        />
                      </TouchableOpacity>
                    );
                  })}
                </View>
                <TouchableOpacity
                  onPressIn={() => props.getData(data.images[5])}
                  onPressOut={() => props.getData(null)}
                  style={{ marginLeft: 2 }}
                >
                  <Image source={data.images[5]} style={{ width: 129, height: 300 }} />
                </TouchableOpacity>
              </View>
            )}
            {data.id === 2 && (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <TouchableOpacity
                  onPressIn={() => props.getData(data.images[2])}
                  onPressOut={() => props.getData(null)}
                  style={{ paddingRight: 2 }}
                >
                  <Image
                    source={data.images[2]}
                    style={{
                      width: 260,
                      height: 300,
                    }}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    width: 130,
                    justifyContent: 'space-between',
                  }}
                >
                  {data.images.slice(0, 2).map((imageData, imgIndex) => {
                    return (
                      <TouchableOpacity
                        onPressIn={() => props.getData(imageData)}
                        onPressOut={() => props.getData(null)}
                        style={{ paddingBottom: 2 }}
                      >
                        <Image source={imageData} style={{ width: 129, height: 150 }} />
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            )}
          </>
        );
      })}
    </View>
  );
};

export default SearchContent;
