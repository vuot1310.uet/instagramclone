import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { videoData } from './Database';
import SingleReel from './SingleReel';
import { useState } from 'react';

const ReelsComponent = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const handleChangeIndexValue = ({ index }) => {
    setCurrentIndex(index);
  };
  return (
    <SwiperFlatList
      data={videoData}
      onChangeIndex={handleChangeIndexValue}
      vertical={true}
      renderItem={({item, index}) => (
        <SingleReel item={item} index={index} currentIndex={currentIndex} />
      )}
      keyExtractor={(item, index) => index}
    />
  );
};

export default ReelsComponent;
