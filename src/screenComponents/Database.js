export const videoData = [
    {
        video: require('../../assets/storage/videos/video1.mp4'),
        title: 'video 1',
        description: 'Feel the buity of nature',
        likes: '245k',
        isLike: false,
    },
    {
        video: require('../../assets/storage/videos/video2.mp4'),
        title: 'video 2',
        description: 'Feel the buity of nature',
        likes: '245k',
        isLike: false,
    },
    {
        video: require('../../assets/storage/videos/video3.mp4'),
        title: 'video 3',
        description: 'Feel the buity of nature',
        likes: '245k',
        isLike: false,
    },
    {
        video: require('../../assets/storage/videos/video4.mp4'),
        title: 'video 4',
        description: 'Feel the buity of nature',
        likes: '245k',
        isLike: false,
    },
];